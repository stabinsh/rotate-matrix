"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var commander_1 = require("commander");
var fs = __importStar(require("fs"));
var path = __importStar(require("path"));
var csv = __importStar(require("fast-csv"));
var cli_args_1 = require("./services/cli-args");
var csv_matrix_transformer_1 = require("./services/csv-matrix-transformer");
var cliArgs = new cli_args_1.CliArgs(commander_1.program, process.argv);
var pathToFile = path.resolve(cliArgs.getFile());
var csvMatrixTransformer = new csv_matrix_transformer_1.CsvMatrixTransformer();
fs.createReadStream(pathToFile)
    .pipe(csv.parse())
    .pipe(csv.format())
    .transform(function (_a, next) {
    var id = _a[0], json = _a[1];
    return next(null, csvMatrixTransformer.transform(id, json));
})
    .pipe(process.stdout)
    .on('end', function () { return process.exit(); });
