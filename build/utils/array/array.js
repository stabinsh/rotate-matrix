"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stringToFlatMatrix = exports.toMatrix = exports.isSquareMatrix = exports.arrayToString = exports.toFlatMatrix = exports.copyMatrix = void 0;
var copyMatrix = function (matrix) {
    return matrix.map(function (innerArray) { return innerArray.slice(); });
};
exports.copyMatrix = copyMatrix;
var toFlatMatrix = function (matrix) { return matrix.flat(); };
exports.toFlatMatrix = toFlatMatrix;
var arrayToString = function (matrix) { return matrix.join(', '); };
exports.arrayToString = arrayToString;
var isSquareMatrix = function (flatMatrix) {
    return Number.isInteger(Math.sqrt(flatMatrix.length));
};
exports.isSquareMatrix = isSquareMatrix;
var toMatrix = function (flatMatrix) {
    if (!(0, exports.isSquareMatrix)(flatMatrix)) {
        throw new Error('Flat array does not have a square matrix representation');
    }
    var size = Math.sqrt(flatMatrix.length);
    var matrix = [];
    for (var i = 0; i < size; i++) {
        var row = [];
        for (var j = 0; j < size; j++) {
            row.push(flatMatrix[i * size + j]);
        }
        matrix.push(row);
    }
    return matrix;
};
exports.toMatrix = toMatrix;
var stringToFlatMatrix = function (jsonString) {
    var flatMatrix = JSON.parse(jsonString);
    if (!Array.isArray(flatMatrix)) {
        throw new Error('Please provide corretly formatted flat matrix a string');
    }
    return flatMatrix;
};
exports.stringToFlatMatrix = stringToFlatMatrix;
