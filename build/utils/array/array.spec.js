"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var array_1 = require("./array");
describe('array utils', function () {
    it('should convert array to string', function () {
        var array = [1, 2, 3, 4];
        var arrayAsString = (0, array_1.arrayToString)(array);
        expect(arrayAsString).toBe('1, 2, 3, 4');
    });
    it('should convert flatMatrix to matrix', function () {
        var flatMatrix = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        var matrix = (0, array_1.toMatrix)(flatMatrix);
        expect(matrix).toStrictEqual([
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
        ]);
    });
    it('should throw error when converting a flat matrix which is not squared to a matrix', function () {
        var flatMatrix = [1, 2, 3, 4, 5, 6, 7, 8];
        expect(function () {
            (0, array_1.toMatrix)(flatMatrix);
        }).toThrow();
    });
    it('should convert matrix to flat matrix', function () {
        var matrix = [
            [1, 2],
            [3, 4],
        ];
        expect((0, array_1.toFlatMatrix)(matrix)).toStrictEqual([1, 2, 3, 4]);
    });
    it('should copy a matrix', function () {
        var matrix = [
            [1, 2],
            [1, 2],
        ];
        expect((0, array_1.copyMatrix)(matrix)).toStrictEqual([
            [1, 2],
            [1, 2],
        ]);
    });
});
