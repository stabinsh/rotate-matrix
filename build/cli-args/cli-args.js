"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CliArgs = void 0;
var CliArgs = /** @class */ (function () {
    function CliArgs(command, args) {
        this.command = command;
        this.processArgs(args);
    }
    CliArgs.prototype.parseArgs = function (args) {
        this.command.requiredOption('-f, --file <path to csv file>', 'path to csv input file you want to process');
        this.command.parse(args);
        return this.command.opts();
    };
    // private validateArgs(args: Args): void {
    //   if ()
    // }
    CliArgs.prototype.setArgs = function (args) {
        this.file = args.file;
    };
    CliArgs.prototype.processArgs = function (args) {
        var parsedArgs = this.parseArgs(args);
        // this.validateArgs(parsedArgs);
        this.setArgs(parsedArgs);
    };
    CliArgs.prototype.getFile = function () {
        return this.file;
    };
    return CliArgs;
}());
exports.CliArgs = CliArgs;
