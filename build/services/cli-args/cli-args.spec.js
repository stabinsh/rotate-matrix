"use strict";
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
var commander_1 = require("commander");
var cli_args_1 = require("./cli-args");
var mockedArgs = ['node', ''];
describe('CliArgs', function () {
    beforeEach(function () {
        jest.resetAllMocks();
    });
    it('should exit process with exit code 1 when --file arg is not provided', function () {
        jest.spyOn(process, 'exit').mockImplementation(function (number) {
            throw new Error("".concat(number));
        });
        expect(function () {
            new cli_args_1.CliArgs(commander_1.program, mockedArgs);
        }).toThrow('1');
    });
    it('should return path to file', function () {
        var fileArg = 'test';
        var cliArgs = new cli_args_1.CliArgs(commander_1.program, __spreadArray(__spreadArray([], mockedArgs, true), ['--file', fileArg], false));
        var file = cliArgs.getFile();
        expect(file).toBe(fileArg);
    });
});
