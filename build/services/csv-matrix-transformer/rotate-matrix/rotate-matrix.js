"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.rotateMatrix = void 0;
var array_1 = require("../../../utils/array");
var Direction = {
    Right: 'Right',
    Left: 'Left',
    Bottom: 'Bottom',
    Top: 'Top',
};
// clockwise rotation direction
var rotateMatrix = function (matrix) {
    // TODO: come up with soltion which doesn't require making copy
    // of existing matrix to optimise space complexity
    var matrixCopy = (0, array_1.copyMatrix)(matrix);
    var row = 0;
    var col = 0;
    var direction = Direction.Right;
    var level = 0;
    while (level < Math.floor(matrix.length / 2)) {
        var currentValue = matrixCopy[row][col];
        switch (direction) {
            case Direction.Right:
                col = col + 1;
                break;
            case Direction.Bottom:
                row = row + 1;
                break;
            case Direction.Left:
                col = col - 1;
                break;
            case Direction.Top:
                row = row - 1;
                break;
            default:
                throw new Error('Please set correction direction using Direction enum');
        }
        matrix[row][col] = currentValue;
        if (row === level && col === matrix.length - 1 - level) {
            direction = Direction.Bottom;
        }
        else if (row === matrix.length - 1 - level &&
            col === matrix.length - 1 - level) {
            direction = Direction.Left;
        }
        else if (row === matrix.length - 1 - level && col === level) {
            direction = Direction.Top;
        }
        else if (row === level && col === level) {
            direction = Direction.Right;
            level = level + 1;
            // set coordinates to start exploring next level
            row = row + 1;
            col = col + 1;
        }
    }
    return matrix;
};
exports.rotateMatrix = rotateMatrix;
