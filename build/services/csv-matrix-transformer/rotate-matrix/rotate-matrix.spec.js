"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rotate_matrix_1 = require("./rotate-matrix");
describe('rotateMatrix', function () {
    it('should rotate 4x4 metric', function () {
        var matrix = [
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12],
            [13, 14, 15, 16],
        ];
        var rotatedMatrix = (0, rotate_matrix_1.rotateMatrix)(matrix);
        expect(rotatedMatrix).toStrictEqual([
            [5, 1, 2, 3],
            [9, 10, 6, 4],
            [13, 11, 7, 8],
            [14, 15, 16, 12],
        ]);
    });
    it('should rotate 3x3 metric', function () {
        var matrix = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
        ];
        var rotatedMatrix = (0, rotate_matrix_1.rotateMatrix)(matrix);
        expect(rotatedMatrix).toStrictEqual([
            [4, 1, 2],
            [7, 5, 3],
            [8, 9, 6],
        ]);
    });
    it('should rotate 2x2 metric', function () {
        var matrix = [
            [40, 20],
            [90, 10],
        ];
        var rotatedMatrix = (0, rotate_matrix_1.rotateMatrix)(matrix);
        expect(rotatedMatrix).toStrictEqual([
            [90, 40],
            [10, 20],
        ]);
    });
    it('should rotate 1x1 metric', function () {
        var matrix = [[-5]];
        var rotatedMatrix = (0, rotate_matrix_1.rotateMatrix)(matrix);
        expect(rotatedMatrix).toStrictEqual([[-5]]);
    });
});
