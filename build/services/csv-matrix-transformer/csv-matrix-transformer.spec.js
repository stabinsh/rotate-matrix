"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var csv_matrix_transformer_1 = require("./csv-matrix-transformer");
describe('CsvMatrixTransformer', function () {
    var csvMatrixTransformer;
    beforeEach(function () {
        csvMatrixTransformer = new csv_matrix_transformer_1.CsvMatrixTransformer();
    });
    it('should transform matrix and format it in scv format', function () {
        var id = '1';
        var jsonString = '[1, 2, 3, 4]';
        expect(csvMatrixTransformer.transform(id, jsonString)).toStrictEqual([
            id,
            "[3, 1, 4, 2]",
            'true',
        ]);
    });
});
