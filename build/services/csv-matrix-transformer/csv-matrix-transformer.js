"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CsvMatrixTransformer = exports.CSV_HEADERS = void 0;
var array_1 = require("../../utils/array");
var rotate_matrix_1 = require("./rotate-matrix");
exports.CSV_HEADERS = ['id', 'json', 'is_valid'];
var CsvMatrixTransformer = /** @class */ (function () {
    function CsvMatrixTransformer() {
    }
    CsvMatrixTransformer.prototype.isHeaderRow = function (id, jsonString) {
        return id === 'id' && jsonString === 'json';
    };
    CsvMatrixTransformer.prototype.getInvalidMatrixRow = function (id) {
        return ["".concat(id, "}"), '[]', 'false'];
    };
    CsvMatrixTransformer.prototype.getRotatedMatrixRow = function (id, flatMatrix) {
        var matrix = (0, array_1.toMatrix)(flatMatrix);
        var rotatedMatrix = (0, rotate_matrix_1.rotateMatrix)(matrix);
        var rotatedFlatMatrix = (0, array_1.toFlatMatrix)(rotatedMatrix);
        return [id, "[".concat((0, array_1.arrayToString)(rotatedFlatMatrix), "]"), 'true'];
    };
    CsvMatrixTransformer.prototype.transform = function (id, jsonString) {
        if (this.isHeaderRow(id, jsonString)) {
            return exports.CSV_HEADERS;
        }
        var flatMatrix = (0, array_1.stringToFlatMatrix)(jsonString);
        if (!(0, array_1.isSquareMatrix)(flatMatrix)) {
            return this.getInvalidMatrixRow(id);
        }
        return this.getRotatedMatrixRow(id, flatMatrix);
    };
    return CsvMatrixTransformer;
}());
exports.CsvMatrixTransformer = CsvMatrixTransformer;
