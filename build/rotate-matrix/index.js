"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.rotateMatrix = void 0;
var rotate_matrix_1 = require("./rotate-matrix");
Object.defineProperty(exports, "rotateMatrix", { enumerable: true, get: function () { return rotate_matrix_1.rotateMatrix; } });
