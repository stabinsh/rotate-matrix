"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.rotateMatrix = void 0;
var matrixToString = function (matrix) { return matrix.join(', '); };
var rotateMatrix = function (matrix, size) {
    if (matrix.length === 1)
        return matrixToString(matrix);
    return matrixToString(matrix);
};
exports.rotateMatrix = rotateMatrix;
