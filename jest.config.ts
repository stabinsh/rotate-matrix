module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  transform: {
    "^.+\\.(ts)$": "ts-jest",
  },
  roots: ['<rootDir>/src/'],
  testMatch: ['**/*.spec.ts'],
  modulePathIgnorePatterns: ["./build/"],
  coveragePathIgnorePatterns: ["./test/mocks.ts"],
  clearMocks: true,
  verbose: true,
  testTimeout: 30000,
};
