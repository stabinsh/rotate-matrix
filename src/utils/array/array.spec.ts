import { arrayToString, copyMatrix, toFlatMatrix, toMatrix } from './array';

describe('array utils', () => {
  it('should convert array to string', () => {
    const array = [1, 2, 3, 4];
    const arrayAsString = arrayToString(array);

    expect(arrayAsString).toBe('1, 2, 3, 4');
  });

  it('should convert flatMatrix to matrix', () => {
    const flatMatrix = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    const matrix = toMatrix(flatMatrix);

    expect(matrix).toStrictEqual([
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ]);
  });

  it('should throw error when converting a flat matrix which is not squared to a matrix', () => {
    const flatMatrix = [1, 2, 3, 4, 5, 6, 7, 8];
    expect(() => {
      toMatrix(flatMatrix);
    }).toThrow();
  });

  it('should convert matrix to flat matrix', () => {
    const matrix = [
      [1, 2],
      [3, 4],
    ];

    expect(toFlatMatrix(matrix)).toStrictEqual([1, 2, 3, 4]);
  });

  it('should copy a matrix', () => {
    const matrix = [
      [1, 2],
      [1, 2],
    ];

    expect(copyMatrix(matrix)).toStrictEqual([
      [1, 2],
      [1, 2],
    ]);
  });
});
