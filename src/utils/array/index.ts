export {
  copyMatrix,
  toMatrix,
  toFlatMatrix,
  arrayToString,
  isSquareMatrix,
  stringToFlatMatrix,
} from './array';
