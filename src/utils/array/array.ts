export const copyMatrix = (matrix: unknown[][]): unknown[][] =>
  matrix.map(innerArray => innerArray.slice());

export const toFlatMatrix = (matrix: unknown[][]): unknown[] => matrix.flat();

export const arrayToString = (matrix: unknown[]): string => matrix.join(', ');

export const isSquareMatrix = (flatMatrix: unknown[]) =>
  Number.isInteger(Math.sqrt(flatMatrix.length));

export const toMatrix = (flatMatrix: unknown[]): unknown[][] => {
  if (!isSquareMatrix(flatMatrix)) {
    throw new Error('Flat array does not have a square matrix representation');
  }

  const size = Math.sqrt(flatMatrix.length);
  const matrix: unknown[][] = [];

  for (let i = 0; i < size; i++) {
    const row: unknown[] = [];

    for (let j = 0; j < size; j++) {
      row.push(flatMatrix[i * size + j]);
    }
    matrix.push(row);
  }
  return matrix;
};

export const stringToFlatMatrix = (jsonString: string): unknown[] => {
  const flatMatrix = JSON.parse(jsonString);

  if (!Array.isArray(flatMatrix)) {
    throw new Error('Please provide corretly formatted flat matrix a string');
  }

  return flatMatrix;
};
