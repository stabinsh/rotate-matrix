import { program } from 'commander';
import * as fs from 'fs';
import * as path from 'path';
import * as csv from 'fast-csv';
import { CliArgs } from './services/cli-args';
import { CsvMatrixTransformer } from './services/csv-matrix-transformer';

const cliArgs = new CliArgs(program, process.argv);
const pathToFile = path.resolve(cliArgs.getFile());
const csvMatrixTransformer = new CsvMatrixTransformer();

fs.createReadStream(pathToFile)
  .pipe(csv.parse())
  .pipe(csv.format<[string, string], string[]>())
  .transform(([id, json], next): void => {
    return next(null, csvMatrixTransformer.transform(id, json));
  })
  .pipe(process.stdout)
  .on('end', () => process.exit());
