import { program } from 'commander';
import { CliArgs } from './cli-args';

const mockedArgs = ['node', ''];

describe('CliArgs', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should exit process with exit code 1 when --file arg is not provided', () => {
    jest.spyOn(process, 'exit').mockImplementation(number => {
      throw new Error(`${number}`);
    });

    expect(() => {
      new CliArgs(program, mockedArgs);
    }).toThrow('1');
  });

  it('should return path to file', () => {
    const fileArg = 'test';
    const cliArgs = new CliArgs(program, [...mockedArgs, '--file', fileArg]);
    const file = cliArgs.getFile();

    expect(file).toBe(fileArg);
  });
});
