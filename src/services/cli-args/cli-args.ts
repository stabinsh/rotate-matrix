import { Command } from 'commander';

interface Args {
  file: string;
}

export class CliArgs {
  private file!: Args['file'];

  constructor(private command: Command, args: string[]) {
    this.processArgs(args);
  }

  private parseArgs(args: string[]): Args {
    this.command.requiredOption(
      '-f, --file <path to csv file>',
      'path to csv input file you want to process'
    );
    this.command.parse(args);
    return this.command.opts<Args>();
  }

  private setArgs(args: Args): void {
    this.file = args.file;
  }

  private processArgs(args: string[]): void {
    const parsedArgs = this.parseArgs(args);
    this.setArgs(parsedArgs);
  }

  getFile(): string {
    return this.file;
  }
}
