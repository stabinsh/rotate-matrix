import {
  isSquareMatrix,
  stringToFlatMatrix,
  toFlatMatrix,
  toMatrix,
  arrayToString,
} from '../../utils/array';
import { rotateMatrix } from './rotate-matrix';

type CSV_ROW = [string, string, string];

export const CSV_HEADERS: CSV_ROW = ['id', 'json', 'is_valid'];

export class CsvMatrixTransformer {
  private isHeaderRow(id: string, jsonString: string): boolean {
    return id === 'id' && jsonString === 'json';
  }

  private getInvalidMatrixRow(id: string): CSV_ROW {
    return [`${id}}`, '[]', 'false'];
  }

  private getRotatedMatrixRow(id: string, flatMatrix: unknown[]): CSV_ROW {
    const matrix = toMatrix(flatMatrix);
    const rotatedMatrix = rotateMatrix(matrix);
    const rotatedFlatMatrix = toFlatMatrix(rotatedMatrix);
    return [id, `[${arrayToString(rotatedFlatMatrix)}]`, 'true'];
  }

  transform(id: string, jsonString: string): CSV_ROW {
    if (this.isHeaderRow(id, jsonString)) {
      return CSV_HEADERS;
    }

    const flatMatrix = stringToFlatMatrix(jsonString);

    if (!isSquareMatrix(flatMatrix)) {
      return this.getInvalidMatrixRow(id);
    }

    return this.getRotatedMatrixRow(id, flatMatrix);
  }
}
