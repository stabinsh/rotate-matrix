import { rotateMatrix } from './rotate-matrix';

describe('rotateMatrix', () => {
  it('should rotate 4x4 metric', () => {
    const matrix = [
      [1, 2, 3, 4],
      [5, 6, 7, 8],
      [9, 10, 11, 12],
      [13, 14, 15, 16],
    ];
    const rotatedMatrix = rotateMatrix(matrix);

    expect(rotatedMatrix).toStrictEqual([
      [5, 1, 2, 3],
      [9, 10, 6, 4],
      [13, 11, 7, 8],
      [14, 15, 16, 12],
    ]);
  });

  it('should rotate 3x3 metric', () => {
    const matrix = [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ];
    const rotatedMatrix = rotateMatrix(matrix);

    expect(rotatedMatrix).toStrictEqual([
      [4, 1, 2],
      [7, 5, 3],
      [8, 9, 6],
    ]);
  });

  it('should rotate 2x2 metric', () => {
    const matrix = [
      [40, 20],
      [90, 10],
    ];

    const rotatedMatrix = rotateMatrix(matrix);

    expect(rotatedMatrix).toStrictEqual([
      [90, 40],
      [10, 20],
    ]);
  });

  it('should rotate 1x1 metric', () => {
    const matrix = [[-5]];

    const rotatedMatrix = rotateMatrix(matrix);

    expect(rotatedMatrix).toStrictEqual([[-5]]);
  });
});
