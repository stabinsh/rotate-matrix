import { CsvMatrixTransformer } from './csv-matrix-transformer';

describe('CsvMatrixTransformer', () => {
  let csvMatrixTransformer: CsvMatrixTransformer;

  beforeEach(() => {
    csvMatrixTransformer = new CsvMatrixTransformer();
  });

  it('should transform matrix and format it in scv format', () => {
    const id = '1';
    const jsonString = '[1, 2, 3, 4]';

    expect(csvMatrixTransformer.transform(id, jsonString)).toStrictEqual([
      id,
      `[3, 1, 4, 2]`,
      'true',
    ]);
  });
});
