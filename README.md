## Prerequisites

1. Node js version 18.3.0 or higher

2. Yarn version 1.22.0 or higher

3. Install dependencies.

```bash
yarn
```

## Run the cli to format a csv file

```bash
node ./build/main.js --file ./test-data/test.csv
```

## Recommended VS Code extensions

- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

### Lint commands

```bash
yarn lint
```

### Build commands

```bash
yarn build
```

### Test commands

- Run unit tests

```bash
yarn test
```

- Run unit tests and watch

```bash
yarn test:watch
```
